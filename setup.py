#! /usr/bin/env python
try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup


setup(
    name="price_comparer",
    version="0.1.0",
    description="Wantsome Project",
    long_description=open("README.md").read(),
    author="Wansome - Python Seria 2",
    url="https://gitlab.com/wantsome-projects/hunt-the-wumpus",
    packages=["price_comparer"],
    entry_points={
        'console_scripts': [
            'hunt-the-wumpus = price_comparer.cmdline:main',
        ],
    },
    requires=open("requirements.txt").readlines(),
)
